import jQuery from 'jquery'
import moment from 'moment'
import Chart from 'chart.js'
import 'fullcalendar'
import axios from 'axios'

// TODO: Read from server side
axios({
  method: 'POST',
  url: '/api/v1/auth/token/generate',
  params: {
    'email': '0934809@hr.nl',
    'password': '123123',
  },
}).then(async (response) => {
  const AUTH_TOKEN = response.data.data.token

  const userHttpResponse = await axios({
    method: 'GET',
    url: '/api/v1/user',
    headers: {
      'Authorization': `Bearer ${AUTH_TOKEN}`,
    },
  })

  let auth = {
    user: userHttpResponse.data.data
  }

  /**
   * Calendar
   */
  const calendarHttpResponse = await axios({
    method: 'GET',
    url: '/api/v1/calendar',
    headers: {
      'Authorization': `Bearer ${AUTH_TOKEN}`,
    },
    params: {
      user_id: auth.user.id,
    },
  })

  const calendarData = calendarHttpResponse.data.data;

  (function($) {
    $('.calendar--attendance').fullCalendar({
      header: {
        left: 'month,agendaWeek,agendaDay',
        center: 'title',
        right: 'today prev,next',
      },
      weekends: false,
      events: calendarData.events,
      eventColor: '#663499',
    })
  })(jQuery)

  /**
   * Dashboard charts
   */
  const chartCanvas = document.querySelector('.chart--attendance-today')

  if (chartCanvas) {
    const attendanceHttpResponse = await axios({
      method: 'GET',
      url: '/api/v1/attendance/index',
      headers: {
        'Authorization': `Bearer ${AUTH_TOKEN}`,
      },
    })

    const attendanceData = attendanceHttpResponse.data.data

    const chartLabelSet = new Set()
    const chartValues = [13, 5, 9, 8, 3, 7, 5]

    attendanceData.forEach((element) => {
      if (element.calendarEvent) {
        chartLabelSet.add(element.calendarEvent.title)
      } else {
        chartLabelSet.add('Uncategorized')
      }
    })

    console.log(chartLabelSet)

    const todayAttendanceChart = new Chart(chartCanvas.getContext('2d'), {
      type: 'bar',
      data: {
        labels: Array.from(chartLabelSet),
        datasets: [{
          label: 'This Quarter\'s Attendance',
          data: chartValues,
          lineTension: 0,
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
})
