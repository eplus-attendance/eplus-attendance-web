'use strict'

const Route = use('Route')

/**
 * Authenticated
 */
Route.group(() => {
  Route.get('/', 'HomeController.index').as('home')

  Route.get('/my-account', 'MyAccountController.index').as('my-account')
  Route.get('/my-account/edit', 'MyAccountController.edit').as('my-account.edit')
  Route.post('/my-account/edit', 'MyAccountController.update').as('my-account.edit')
}).middleware(['auth'])

/**
 * Authentication
 */
  /**
   * Guest
   */
  Route.group(() => {
    Route.get('/auth/register', 'Auth/RegisterController.index').as('auth.register')
    Route.post('/auth/register', 'Auth/RegisterController.register').as('auth.register')
    Route.get('/auth/login', 'Auth/LoginController.index').as('auth.login')
    Route.post('/auth/login', 'Auth/LoginController.login').as('auth.login')
  }).middleware(['guest'])

   /**
    * Authenticated
    */
  Route.group(() => {
    Route.post('/auth/logout', 'Auth/LogoutController.logout').as('auth.logout')
  }).middleware(['auth'])

/**
 * API
 */
Route.group(() => {
  Route.post('/api/v1/auth/token/generate', 'Api/V1/Auth/TokenController.generate')
  Route.post('/api/v1/auth/token/revoke', 'Api/V1/Auth/TokenController.revoke')
  Route.get('/api/v1/user', 'Api/V1/UserController.getCurrent')
  Route.get('/api/v1/calendar', 'Api/V1/CalendarController.index')
  Route.get('/api/v1/attendance/index', 'Api/V1/AttendanceController.index')
  Route.post('/api/v1/attendance/create', 'Api/V1/AttendanceController.create')
})
