'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const User = use('App/Models/User')

class UserSeeder {
  async run () {
    const masterPassword = '123123'

    await User.createMany([
      {
        id: 1,
        username: '0934809',
        email: '0934809@hr.nl',
        password: masterPassword,
      },
      {
        id: 2,
        username: '0932707',
        email: '0932707@hr.nl',
        password: masterPassword,
      },
      {
        id: 3,
        username: 'sitde',
        email: 'sitde@hr.nl',
        password: masterPassword,
       }
    ])
  }
}

module.exports = UserSeeder
