'use strict'

const Card = use('App/Models/Card')

class CardSeeder {
  async run () {
    await Card.createMany([
      {
        user_id: 1,
        uid: 'EBA1AD29',
      },
      {
        user_id: 2,
        uid: 'B659AA59',
      },
      {
        user_id: 3,
        uid: 'TESTCARD',
      }
    ])
  }
}

module.exports = CardSeeder
