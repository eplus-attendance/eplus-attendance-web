'use strict'

const CicoDevice = use('App/Models/CicoDevice')

class CicoDeviceSeeder {
  async run () {
    await CicoDevice.createMany([
      {
        location: 'WD.05.002',
      },
      {
        location: 'WD.05.005',
      },
      {
        location: 'WD.05.018',
      }
    ])
  }
}

module.exports = CicoDeviceSeeder
