'use strict'

const Schema = use('Schema')

class CicoDeviceSchema extends Schema {
  up () {
    this.create('cico_devices', (table) => {
      table.increments()
      table.string('location', 20)
      table.timestamps()
    })
  }

  down () {
    this.drop('cico_devices')
  }
}

module.exports = CicoDeviceSchema
