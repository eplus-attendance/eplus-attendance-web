'use strict'

const Schema = use('Schema')

class AttendanceSchema extends Schema {
  up () {
    this.create('attendances', (table) => {
      table.increments()
      table.integer('card_id').unsigned().references('id').inTable('cards')
      table.integer('cico_device_id').unsigned().references('id').inTable('cico_devices')
      table.integer('calendar_event_id').unsigned().references('id').inTable('calendar_events')
      table.boolean('is_checked_in').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('attendances')
  }
}

module.exports = AttendanceSchema
