'use strict'

const Schema = use('Schema')

class CalendarEventSchema extends Schema {
  up () {
    this.create('calendar_events', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('title', 60).notNullable()
      table.string('description')
      table.string('location')
      table.string('uid').unique()
      table.timestamp('starts_at')
      table.timestamp('ends_at')
      table.timestamps()
    })
  }

  down () {
    this.drop('calendar_events')
  }
}

module.exports = CalendarEventSchema
