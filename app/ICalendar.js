'use strict'

const moment = use('moment')
const Axios = use('Axios')
const User = use('App/Models/User')
const CalendarEvent = use('App/Models/Calendar/CalendarEvent')
const CalendarException = use('App/Exceptions/CalendarException')

class ICalendar {
  static async findByUsername(username) {
    const iCalendar = new ICalendar()

    iCalendar.parserUrl = 'https://ical-to-json.herokuapp.com/convert.json?url='
    iCalendar.icalUrl = `https://hint.hr.nl/xsp/rooster/iCal.xsp/${username}-1710783.ics`
    iCalendar.events = []

    await iCalendar.setUser(username)
    await iCalendar.parseAndSetICalendarData()

    return iCalendar
  }

  async parseAndSetICalendarData() {
    try {
      const response = await Axios({
        method: 'GET',
        url: this.parserUrl + this.icalUrl,
        responseType: 'text',
      })

      this._data = response.data.vcalendar[0]
      this.title = this._data['x-wr-calname']
      await this.createUnregisteredEvents()
    } catch(error) {
      throw new CalendarException(error)
    }
  }

  async setUser(username) {
    this.user = await User.findBy('username', username)
  }

  async createUnregisteredEvents() {
    for (const event of this._data.vevent) {
      const eventObject = await CalendarEvent.findOrCreate({ uid: event.uid }, {
        user_id: this.user.id,
        title: event.summary,
        description: event.description,
        location: event.location,
        starts_at: moment(event.dtstart),
        ends_at: moment(event.dtend),
        uid: event.uid,
      })

      this.events.push(eventObject)
    }
  }
}

module.exports = ICalendar
