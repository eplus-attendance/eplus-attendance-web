'use strict'

const Model = use('Model')

const md5 = require('md5')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     *
     * Look at `app/Models/Hooks/User.js` file to
     * check the hashPassword method
     */
    this.addHook('beforeSave', 'User.hashPassword')
  }

  static get hidden () {
    return [
      'password',
    ]
  }

  static get computed () {
    return [
      'avatar'
    ]
  }

  static castDates (field, value) {
    if (field === 'updated_at') {
      return `${value.fromNow(true)} ago`
    }

    return super.formatDates(field, value)
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  events() {
    return this.hasMany('App/Models/Calendar/Event')
  }

  cards() {
    return this.hasMany('App/Models/Card')
  }

  getAvatar ({ email }) {
    return `https://www.gravatar.com/avatar/${md5(email)}?s=100&d=mm`
  }
}

module.exports = User
