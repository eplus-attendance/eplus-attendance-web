'use strict'

const Model = use('Model')

class CicoDevice extends Model {
  attendances() {
    return this.hasMany('App/Models/Attendance')
  }
}

module.exports = CicoDevice
