'use strict'

const Model = use('Model')
const Attendance = use('App/Models/Attendance')

class Card extends Model {
  async isCheckingInToCicoDevice(cicoDeviceId) {
    const attendances = await Attendance.query()
      .where('card_id', this.id)
      .orderBy('id', 'DESC')
      .limit(1)
      .fetch()

    const lastAttendance = attendances.first()

    if (!lastAttendance) {
      return true
    }

    // TODO: Base on `cico_device`.`location`
    if (lastAttendance.cico_device_id != cicoDeviceId) {
      return true
    }

    return (lastAttendance.isCheckedOutOrExpired())
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  attendances() {
    return this.hasMany('App/Models/Attendance')
  }
}

module.exports = Card
