'use strict'

const Model = use('Model')

class CalendarEvent extends Model {
  static get dates () {
    return super.dates.concat(['starts_at', 'ends_at'])
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = CalendarEvent
