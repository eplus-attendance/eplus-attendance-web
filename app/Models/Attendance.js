'use strict'

const Model = use('Model')
const moment = use('moment')

class Attendance extends Model {
  isCheckedOutOrExpired() {
    return (this.isCheckedOut() || this.isExpired())
  }

  isCheckedOut() {
    return !(this.is_checked_in)
  }

  isExpired() {
    const checkedInMoment = moment(this.created_at)

    // TODO: Change to end of related event
    const expirationMoment = checkedInMoment.add(4, 'hours')

    return (checkedInMoment.isAfter(expirationMoment))
  }

  card() {
    return this.belongsTo('App/Models/Card')
  }

  cicoDevice() {
    return this.belongsTo('App/Models/CicoDevice')
  }

  calendarEvent() {
    return this.belongsTo('App/Models/Calendar/CalendarEvent')
  }
}

module.exports = Attendance
