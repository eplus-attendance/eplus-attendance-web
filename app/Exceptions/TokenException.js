'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

class TokenException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  // handle () {}
}

module.exports = TokenException
