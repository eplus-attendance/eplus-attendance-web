'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

class CalendarException extends LogicalException {
  /**
   * Handle this exception by itself
   */
  // handle () {}
}

module.exports = CalendarException
