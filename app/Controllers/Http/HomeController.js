'use strict'

const ICalendar = use('App/ICalendar')

class HomeController {
  async index ({ view, request, auth }) {
    const user = await auth.getUser();

    const iCalendar = await ICalendar.findByUsername(user.username)

    return view.render('index', {
      iCalendar,
    })
  }
}

module.exports = HomeController
