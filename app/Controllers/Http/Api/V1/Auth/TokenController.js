'use strict'

class TokenController {
  static async validate ({ auth }) {
    try {
      await auth.authenticator('api').check()
    } catch (e) {
      throw new TokenException('Authentication failed. Invalid token perhaps?')
    }
  }

  async generate ({ request, response, auth }) {
    const { email, password } = request.all()

    try {
      const authData = await auth.authenticator('api').attempt(email, password)

      return response.status(200).json({
        message: 'OK',
        data: authData,
      })
    } catch (e) {
      return response.status(401).json({
        message: 'Failed authentication',
        exception: e.message,
      })
    }
  }

  async revoke ({ response, auth }) {
    this.constructor.validate({ response, auth })

    try {
      // const user = await auth.getUser()
      // const revocation = await auth.authenticator('api').revokeTokensForUser(user)
      await auth.authenticator('api').revokeTokens()

      return response.status(200).json({
        message: 'OK',
      })
    } catch (e) {
      return response.status(400).json({
        message: 'Could not revoke token. Does it exist?',
        exception: e.message,
      })
    }
  }
}

module.exports = TokenController
