
'use strict'

const TokenController = use('App/Controllers/Http/Api/V1/Auth/TokenController')

class UserController {
  async getCurrent ({ response, auth }) {
    TokenController.validate({ response, auth })

    const userInfo = await auth.authenticator('api').getUser()

    return response.status(200).json({
      data: userInfo,
    })
  }
}

module.exports = UserController
