'use strict'

const User = use('App/Models/User')
const CalendarEvent = use('App/Models/Calendar/CalendarEvent')

class CalendarController {
  async index ({ request, response }) {
    const rawEvents = await CalendarEvent.query()
      .where('user_id', request.get().user_id)
      .fetch()

    const formattedEvents = []

    rawEvents.toJSON().forEach((event) => {
      formattedEvents.push({
        title: event.title,
        allDay: false,
        start: event.starts_at,
        end: event.ends_at,
      })
    })

    return response.json({
      data: {
        events: formattedEvents,
      },
    })
  }
}

module.exports = CalendarController
