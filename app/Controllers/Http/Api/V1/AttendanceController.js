'use strict'

const Attendance = use('App/Models/Attendance')
const Card = use('App/Models/Card')
const CalendarEvent = use('App/Models/Calendar/CalendarEvent')
const TokenController = use('App/Controllers/Http/Api/V1/Auth/TokenController')

class AttendanceController {
  async index ({ response, auth }) {
    TokenController.validate({ response, auth })

    try {
      const user = await auth.authenticator('api').getUser()
      const card = await user.cards().first()

      const attendances = await card.attendances()
        .with('card')
        .with('cicoDevice')
        .with('calendarEvent')
        .fetch()

      return response.status(200).json({
        data: attendances.toJSON(),
      })
    } catch (e) {
      return response.status(500).json({
        message: 'Uh oh. Something went wrong.',
        exception: e.message,
      })
    }
  }

  async create ({ request, response }) {
    const cicoDeviceId = request.get().cico_device_id
    const cardUid = request.get().card_uid

    const card = await Card.findBy('uid', cardUid)
    const event = await CalendarEvent.query()
      .whereRaw('NOW() >= starts_at')
      .whereRaw('NOW() < ends_at')
      .first()

    const attendance = new Attendance()
    attendance.cico_device_id = cicoDeviceId
    attendance.card_id = card.id

    if (event) {
      attendance.calendar_event_id = event.id
    }

    attendance.is_checked_in = await card.isCheckingInToCicoDevice(cicoDeviceId)

    await attendance.save()

    return response.status(201).json({
      data: attendance,
    })
  }
}

module.exports = AttendanceController
