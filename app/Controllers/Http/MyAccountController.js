'use strict'

const { validateAll } = use('Validator')

class MyAccountController {
  async index({ view, request, auth }) {
    const user = await auth.getUser()

    return view.render('my-account/index', {
      user
    })
  }

  async edit({ view, request, auth }) {
    const user = await auth.getUser()

    return view.render('my-account/edit', {
      user
    })
  }

  async update({ view, request, response, session, auth }) {
    const user = await auth.getUser()

    const { email, password } = request.all()

    const rules = {
      email: `required|email|unique:users,email,id,${user.id}`,
      password: 'min:6',
    }

    const validation = await validateAll(request.all(), rules)

    if (validation.fails()) {
      session.withErrors(validation.messages())
        .flashAll()

      return response.redirect('back')
    }

    let userData = {
      email,
    }

    if (password) {
      userData.password = password
    }

    user.merge(userData)

    await user.save()

    return response.route('my-account')
  }
}

module.exports = MyAccountController
